﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {
	[SerializeField]
	private GameManager gm;

	[SerializeField]
	private MenuSettings Mset;

	//[SerializeField]
	private bool walking;

	private Animator cameraMovement;

	private Vector3 startPosition;
	private Vector3 playerPosition;
	private float xPos;
	private Vector3 pos1, pos2, offset, moveTo;


	//[SerializeField]
	//public 
	private float playerSpeed;

	private int timeGo;
	private float playerPosZ;
	[SerializeField]
	private Text TimeText;
	[SerializeField]
	private Text BestResult;

	// Use this for initialization
	void Start () {
		//startPlayer ();
		startPosition = transform.position;
		cameraMovement = GetComponentInChildren<Animator>();
	}

	public void Update() {
		if (IsWalking()) {
			PlayerMovement ();

		}
	}

	public void startPlayer(){
		

		playerPosition = transform.position;
		playerPosZ = playerPosition.z;
		playerSpeed = Mset.GetPlayerSpeed();
		//Debug.Log ("Speed = " + playerSpeed);
		timeGo = 0;
		PlayerMovement ();
		SetBestResult ();

		SetTimeText();
	}

	public void setTime(){
		if (timeGo > PlayerPrefs.GetInt ("Corridor")) {
			PlayerPrefs.SetInt ("Corridor", timeGo);
			SetBestResult ();
		}
	}

	void SetTimeText(){
		TimeText.text = timeGo.ToString() + " m";
	}

	void SetBestResult(){
		BestResult.text = "Best Result:" + PlayerPrefs.GetInt ("Corridor") + " m";
	}

	public void reset(){
		SetBestResult ();
		timeGo = 0;
		ResetPlayerPosition ();
		SetTimeText ();
	}

	public void PlayerMovement(){
		// Move camera forward
		timeGo++;
		SetTimeText();

		playerPosition.z += playerSpeed;

		transform.position = playerPosition;
	
		float distanceWalked = Vector3.Distance(transform.position, startPosition);
	
		if ((distanceWalked >= Mset.GetResetDistance())||(distanceWalked>40)) {
			ResetPlayerPosition ();
		}

	}
		
	public void StartWalking(){
		walking = true;
		cameraMovement.SetBool ("playerWalking",true);
	}

	public void StopWalking(){
		walking = false;
		cameraMovement.SetBool ("playerWalking",false);
	}

	public bool IsWalking(){
		return walking;
	}

	private void ResetPlayerPosition(){
		// Reset player position
		Debug.Log("Start Position "+startPosition);
		playerPosition = startPosition;
		//playerPosition=0;

		//Camera.main.transform.position = playerPosition;
		//Debug.Log("RESET PLAYER/CAMERA POSITION");
	}

}
