﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

//	private Vector3 startPosition;
//
//	[SerializeField]
//	private float resetDistance;
//
//	private Vector3 playerPosition;

	//[SerializeField]
	//private float playerSpeed;

	[SerializeField]
	private Monster monster;

	[SerializeField]
	private Player player;

	[SerializeField]
	private GameObject buttonReset;

	[SerializeField]
	private MenuSettings Mset;

	[SerializeField]
	private AudioSource music;

	[SerializeField]
	private AudioClip screemSfx;
	[SerializeField]
	private AudioClip pianoSfx;

	public float reactionTime=0;
	//private float varMonster=0;
	//private bool going;


	private bool MenuOn;
	public Canvas menuCanvas;
	public Canvas BaseCanvas;
	//public Camera cameraView;

	//private Vector3 pos1, pos2, offset, moveTo;

	//[SerializeField]
	//private float varMonsterLimit;

	//[SerializeField]
	//public 
	private float reactionTimeLimit;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
	// Use this for initialization
	void Start () {
		menuCanvas = menuCanvas.GetComponent<Canvas> ();
		menuCanvas.enabled = false;
		reactionTimeLimit = Mset.GetReactLimit ();
		StartGame ();
	}


	public void StartGame(){
		player.startPlayer ();
		//ResetGame ();
	}

	// Update is called once per frame
	void Update () {
		// Player holds finger on screen
		if ((Input.GetMouseButtonDown (0))&&(!MenuOn)) {
			player.StartWalking ();
		} else if (Input.GetMouseButtonUp (0)) {
			player.StopWalking();
		}
			
		if (Input.GetKey("escape"))
			Application.Quit();
		
		if (player.IsWalking ()) {
			if (monster.ComingMonster (true)) {
				//monster.ComingMonster (true);
				reactionTime++;
				//Debug.Log ("S I L E N C E.......... ");
				if (music.isPlaying) {
					music.Pause ();
				}
				if (reactionTime > Mset.GetReactLimit ()) {
					GoMonster ();
					monster.varMonster = 0;
				}
			}
		}
		else {	
			if (monster.ComingMonster (false)) {
				// monster coming..
				//monster.ComingMonster (false);
				Debug.Log ("Good... " + reactionTime);
				music.Play ();
				monster.varMonster = 0;
				reactionTime = 0;
				Mset.SetMonsterLimit (Mset.GetMonsterLimit () - 1);
				reactionTimeLimit = reactionTimeLimit - 5;
				if (reactionTimeLimit < 0) {
					ResetGame ();
				}
			}
				// Player does not hold finger on screen
		}
	
		// To measure time
		//reactionTime += Time.deltaTime;

	}

	public void GoMonster(){
		
		player.StopWalking ();
		monster.GetComponent<Animator> ().Play ("MonsterShow");
		buttonReset.GetComponent<Animator> ().Play ("btnResetShown");
		//monster.activeSelf ();
		music.clip = screemSfx;
		music.volume = 1;
		music.loop = false;
		music.Play();
		//buttonReset.GetComponent<Animator> ().Play ("btnResetShown");
		//buttonReset.activeSelf ();
		player.setTime();
		
		reactionTime=0;
	}



	public void ResetGame(){
		Debug.Log ("RESTART GAME");
		monster.GetComponent<Animator> ().Play ("MonsterHidden");
		buttonReset.GetComponent<Animator> ().Play ("btnResetHidden");
		//monster.activeSelf ();
		Mset.SetMonsterLimit(10);
		reactionTimeLimit = Mset.GetReactLimit();

		music.clip = pianoSfx;
		music.volume = 1;
		music.loop = true;
		music.Play();
		player.reset ();
		StartGame ();
		//player.going = true;
		Update ();
	}

	public void ShowMenu(){
		menuCanvas.enabled = true;
		BaseCanvas.enabled = false;
		player.StopWalking ();
		MenuOn = true;
	}

	public void QuitSettings() {
		menuCanvas.enabled = false;
		BaseCanvas.enabled = true;
		MenuOn = false;
		//player.going = true;
	}

}
