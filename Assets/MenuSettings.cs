﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuSettings : MonoBehaviour {

	[SerializeField]
	private GameManager gm;

	[SerializeField]
	private InputField ResetDistance;

	[SerializeField]
	private InputField PlayerSpeed;

	[SerializeField]
	private InputField MonsterAppear;

	[SerializeField]
	private InputField ReactLimit;

	[SerializeField]
	private float varMonsterLimit;

	// Use this for initialization
	void Start () {
		
		try
		{
			ResetDistance.text = PlayerPrefs.GetFloat ("CorridorResetDist").ToString();

		}
		// handle the error
		catch (System.Exception err)
		{
			Debug.Log("Got: " + err);
		}
		try
		{
			PlayerSpeed.text = PlayerPrefs.GetFloat ("CorridorPlayerSpeed").ToString();
			Debug.Log ("Speed MSet Start = " + PlayerPrefs.GetFloat ("CorridorPlayerSpeed").ToString());
		}
		// handle the error
		catch (System.Exception err)
		{
			Debug.Log("Got: " + err);
		}
		try
		{
			MonsterAppear.text = PlayerPrefs.GetInt ("CorridorMonsterApp").ToString();
			Debug.Log ("MonsterApp MSet Start = " + PlayerPrefs.GetInt ("CorridorMonsterApp").ToString());
		}
		// handle the error
		catch (System.Exception err)
		{
			Debug.Log("Got: " + err);
		}
		try
		{
			ReactLimit.text = PlayerPrefs.GetFloat ("CorridorReactLimit").ToString();
			Debug.Log ("React Limit MSet Start = " + PlayerPrefs.GetFloat ("CorridorReactLimit").ToString());
		}
		// handle the error
		catch (System.Exception err)
		{
			Debug.Log("Got: " + err);
		}

	}

	public float GetResetDistance(){
		try
		{
			return PlayerPrefs.GetFloat ("CorridorResetDist");

		}
		// handle the error
		catch (System.Exception err)
		{
			return float.Parse(ResetDistance.text);
			Debug.Log("Got: " + err);
		}
	}

	public float GetMonsterLimit(){
		//Debug.Log ("Monster Limit get = " + varMonsterLimit);
		return varMonsterLimit;

	}

	public void SetMonsterLimit(float x){
		varMonsterLimit = x;

		PlayerPrefs.SetFloat ("CorridorMonserLimit", x);
	}

	public float GetPlayerSpeed(){
		try
		{
			return PlayerPrefs.GetFloat ("CorridorPlayerSpeed");

		}
		// handle the error
		catch (System.Exception err)
		{
			return float.Parse(PlayerSpeed.text);
			Debug.Log("Got: " + err);
		}
	}

	public int GetMonsterAppear(){
		try
		{
			return PlayerPrefs.GetInt ("CorridorMonsterApp");

		}
		// handle the error
		catch (System.Exception err)
		{
			return int.Parse(MonsterAppear.text);
			Debug.Log("Got: " + err);
		}
	}

	public float GetReactLimit(){
		try
		{
			return PlayerPrefs.GetFloat ("CorridorReactLimit");

		}
		// handle the error
		catch (System.Exception err)
		{
			return float.Parse(ReactLimit.text);
			Debug.Log("Got: " + err);
		}
	}

	// Update is called once per frame
	public void SaveSettings(){
		PlayerPrefs.SetFloat ("CorridorResetDist", float.Parse(ResetDistance.text));

		PlayerPrefs.Save ();
		PlayerPrefs.SetFloat ("CorridorPlayerSpeed", float.Parse(PlayerSpeed.text));

		PlayerPrefs.Save ();
		PlayerPrefs.SetInt ("CorridorMonsterApp", int.Parse(MonsterAppear.text));

		PlayerPrefs.Save ();
		PlayerPrefs.SetFloat ("CorridorReactLimit", float.Parse(ReactLimit.text));

		PlayerPrefs.Save ();
		gm.ResetGame();
		gm.QuitSettings ();
	}
}
