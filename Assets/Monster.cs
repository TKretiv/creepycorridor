﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour {

	[SerializeField]
	private GameManager gm;

	[SerializeField]
	private MenuSettings Mset;

	public float varMonster=0;

	//[SerializeField]
	private int monsterAppearRate;

	// Use this for initialization
	void Start () {
		monsterAppearRate = Mset.GetMonsterAppear ();
		//Debug.Log("Monster app = "+monsterAppearRate);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public bool ComingMonster(bool press){
		//RandomRange random = new RandomRange();
		if (press) {
			int randomNumber = Random.Range (0, 10000);
			monsterAppearRate = Mset.GetMonsterAppear ();
			//Debug.Log ("Monster app  2  = " + monsterAppearRate);
			int modRand = randomNumber % monsterAppearRate;

			if (modRand == 0) {
				varMonster++;
				//Debug.Log ("ReactionTime: " + reactionTime + " - " + randomNumber);
			}
			if (varMonster > Mset.GetMonsterLimit ()) {
				return true;
			} else {
				return false;
			}
		
			/*if ((varMonster > Mset.GetMonsterLimit ()) & (gm.reactionTime > Mset.GetReactLimit ())) {
				gm.GoMonster ();
				varMonster = 0;
			}*/
		} else {
			if (varMonster > Mset.GetMonsterLimit ()) {
				return true;
			} else {
				return false;
			}
		}
	}

}
